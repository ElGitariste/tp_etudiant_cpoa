package test;

import static org.junit.Assert.assertEquals;

import java.util.Map.Entry;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import tp.Formation;

public class FormationTest {
	Formation f;
	
	@Before
	public void init() {
		f = new Formation(1);
		f.ajouterMatiere("CPOA",3);
	}

	
	@Test
	public void test_obtenir_coeff() {
		f.ajouterMatiere("Maths", 2);
		double test = f.getCoeff("Maths");
		assertEquals("test", 2, test, 0.01);
	}
	@Test
	public void test_supprimer_matiere(){
		f.suppMatiere("CPOA");
		assertEquals("on ne peut pas avoir de coeff pour CPOA",null,f.getmCoeff().get("CPOA"));
	}
	
	@Test
	public void test_obtenir_id_formation(){
		f.ajouterMatiere("Maths", 2);
		assertEquals("on devrait avoir 1",1,f.getIdf());
	}
	
	
	

}
