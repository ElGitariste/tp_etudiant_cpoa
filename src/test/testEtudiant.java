package test;
 
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import tp.Etudiant;
import tp.Formation;
import tp.Identite;


public class testEtudiant {
	Identite id;
	Etudiant etu;
	
	
	@Before
	public void init() {
		id = new Identite("1", "Fredo", "Mercure");
		Formation f = new Formation(1);
		f.getmCoeff().put("Maths", 1.00);
		f.getmCoeff().put("Histoire", 2.00);
		etu = new Etudiant(id, f);
	}
	
	@Test
	public void testAjout() {
		etu.ajouterNote("Maths", 12);
		etu.ajouterNote("Maths", 20);
		ArrayList<Double> res = new ArrayList<Double>();
		res.add(12.00);
		res.add(20.00);
		assertEquals("On doit avoir une liste contenant 12 et 20", etu.getmNote("Maths"), res);
	}
	
	@Test
	public void testMauvaisAjout() {
		etu.ajouterNote("Maths", -2);
		etu.ajouterNote("Maths", 21);
		ArrayList<Double> res = null;

		assertEquals("On doit avoir une liste retournant null", etu.getmNote("Maths"), res);
	}
	
	@Test
	public void testCalculMoy() {
		etu.ajouterNote("Maths", 12);
		etu.ajouterNote("Maths", 16);
		assertEquals("La Moyenne devrait etre egale a 14", etu.calculMoy("Maths"), 14,0.00);
		System.out.println(etu.calculMoy("Maths"));
	}
	
	@Test
	public void testCalculMoyGen() {
		etu.ajouterNote("Maths", 12);
		etu.ajouterNote("Maths", 16);
		etu.ajouterNote("Histoire", 14);
		assertEquals("La Moyenne devrait etre egale a 14", etu.calculMoyGen(), 14.0,0.00);
	}
	

	
	

	
	
	
	
	
	

}
