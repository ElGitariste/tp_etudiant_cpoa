package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import tp.Etudiant;
import tp.Formation;
import tp.Groupe;
import tp.Identite;

public class TestGroupe {
	Identite id;
	Identite id2;
	Etudiant etu;
	Etudiant etu2;
	Groupe grp;
		
		
	@Before
	public void init() {
		id = new Identite("1", "Fredo", "Mercure");
		id2 = new Identite("2", "Bernard", "Kadila");
		Formation f = new Formation(1);
		f.getmCoeff().put("Maths", 1.00);
		etu = new Etudiant(id, f);
		etu2 = new Etudiant(id2, f);
		etu.ajouterNote("Maths", 12);
		etu.ajouterNote("Maths", 20);
		etu2.ajouterNote("Maths", 3);
		etu2.ajouterNote("Maths", 3);	
		grp = new Groupe(f);
		grp.ajouterEtu(etu);
		grp.ajouterEtu(etu2);
	}
		
	
	@Test
	public void tesstTriParMerite() {
		ArrayList<Etudiant> res = new ArrayList<Etudiant>();
		grp.triParMerite();
		res.add(etu);
		res.add(etu2);
		assertEquals("fg", grp.getEtu(), res);
	}
	
	@Test
	public void tesstTriAlpha() {
		ArrayList<Etudiant> res = new ArrayList<Etudiant>();
		grp.triAlpha();
		res.add(etu2);
		res.add(etu);
		assertEquals("fg", grp.getEtu(), res);
	}
	
}
