package tp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Groupe {

	private ArrayList<Etudiant> etu;
	private Formation f;

	public Groupe(Formation f){
		this.etu = new ArrayList<Etudiant>();
		this.f = f;
	}

	public void ajouterEtu(Etudiant e){
		this.etu.add(e);
	}

	public void supprimerEtu(Etudiant e){
		this.etu.remove(e);
	}

	public double calculMoyMatiere(ArrayList<Etudiant> e,String matiere){
		double moyEtu = 0.0;
		double moyGroupe = 0.0;
		for(int i =0; i<e.size();i++){
			moyEtu=e.get(i).calculMoy(matiere);
			moyGroupe+= moyEtu;
		}
		return moyGroupe;
	}

	public double calculMoyGroupe(ArrayList<Etudiant> e){
		double moyGen = 0.0;
		double moyGenGroupe = 0.0;
		for(int i = 0; i<e.size(); i++){
			moyGen =e.get(i).calculMoyGen();
			moyGenGroupe += moyGen;
		}
		return moyGenGroupe;
	}

	public void triParMerite(){
		Collections.sort(etu,new Comparator<Etudiant>(){

			@Override
			public int compare(Etudiant e1, Etudiant e2) {
				return (int)(e2.calculMoyGen() -e1.calculMoyGen());
			}

		});
	}

	public void triAlpha(){
		Collections.sort(etu,new Comparator<Etudiant>(){

			@Override
			public int compare(Etudiant e1, Etudiant e2) {
				return (e1.getNom().compareTo(e2.getNom()));
			}

		}
		);
	}
	
	public Etudiant getEtudiant(int indice){
		
		System.out.println(etu.get(indice));
		return etu.get(indice);
	}

	public ArrayList<Etudiant> getEtu() {
		return etu;
	}

	public void setEtu(ArrayList<Etudiant> etu) {
		this.etu = etu;
	}

	public Formation getF() {
		return f;
	}

	public void setF(Formation f) {
		this.f = f;
	}





}
