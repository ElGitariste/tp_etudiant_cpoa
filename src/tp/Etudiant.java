package tp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class Etudiant {
	private Identite id;
	private Formation form;
	private HashMap<String,ArrayList<Double>> mNote;
	
	public Etudiant(Identite id, Formation formation){
		this.id = id;
		this.form = formation;
		mNote = new HashMap<String,ArrayList<Double>>();
	}
	
	public void ajouterNote(String matiere ,double note){
		
		if(note <= 20 && note >= 0){
			//On verifie que la matiere est bien dans la formation de l etudiant
			if(this.form.getmCoeff().containsKey(matiere)){
				if(!this.mNote.containsKey(matiere)){
					ArrayList<Double> newnote = new ArrayList<Double>();
					newnote.add(note);
					mNote.put(matiere, newnote);
				}
			
				else {
				
					mNote.get(matiere).add(note);
					
				}
			}
		}		
	}
	
	public double calculMoy(String matiere){
		double res = 0;
		//verifie que la matiere existe
		if(mNote.containsKey(matiere)==true){
			
		ArrayList<Double> listeNote =mNote.get(matiere);
		for(int i = 0; i< listeNote.size() ; i++){
			res= res + listeNote.get(i)/ listeNote.size();
		}
		
		return res;
		}
		else{
			return (Double) null;
		}
	}
	
	public double calculMoyGen(){
		Set<String> listeMatiere = mNote.keySet();
		double somme = 0;
		double divis = 0;
		double moyenne = 0;
		for(String matiere : listeMatiere){
			Double moyMat = calculMoy(matiere);
			Double coef = form.getCoeff(matiere);
			somme = somme + (moyMat * coef);
			divis = divis + (1 * coef); 
			}
		
		moyenne = somme / divis;
		return moyenne ;
		}
		
	public String getNom(){
		return id.getNom();
	}
	
	public ArrayList<Double> getmNote(String matiere){
	return mNote.get(matiere);	
		
	}
}
