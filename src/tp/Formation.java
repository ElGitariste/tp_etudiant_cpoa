package tp;
import java.util.HashMap;

public class Formation {

	private int idf;
	private HashMap<String,Double> mCoeff;

	public Formation(int id) {
		this.mCoeff = new HashMap<String, Double>();
		this.idf = id;
	}
	
	public void ajouterMatiere(String mat, int coeff) {
		if(coeff > 0)
		mCoeff.put(mat, (double) coeff);
	}
	
	public void suppMatiere(String mat) {
		mCoeff.remove(mat);
	}
	
	public double getCoeff(String mat) {
		double res = -1;
		res = mCoeff.get(mat);
		if(res == -1) {
			return ((Double) null);
		}
		return res;
	}

	public int getIdf() {
		return idf;
	}

	public void setIdf(int idf) {
		this.idf = idf;
	}

	public HashMap<String, Double> getmCoeff() {
		return mCoeff;
	}

	public void setmCoeff(HashMap<String, Double> mCoeff) {
		this.mCoeff = mCoeff;
	}
	
	
	
}
