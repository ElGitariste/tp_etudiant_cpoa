package tp;

public class Identite {

	private String nip;
	private String prenom;
	private String nom;
	
	public Identite(String numIdentif , String pre , String name){
		this.nip = numIdentif;
		this.prenom = pre;
		this.nom = name;
	}
	
	public String getNom(){
		return this.nom;
	}
}
